#include <stdio.h>
#include <string.h>
 
int main()
{

    //variables
  	char Texto[100], caracter;
  	int i;
    int j = 0;
    char TextoReemplazado[100];
 
    //Pide el texto al usuario y lo guarda
  	printf("\n Ingrese su texto: ");
  	fgets(Texto, 100, stdin);
  	
    //Pide un caracter para eliminar
  	printf("\n Ingrese un caracter a eliminar: ");
  	scanf("%c", &caracter);
  	
  	//Bucle para eliminar el caracter del texto
  	for(i = 0; i < strlen(Texto); i++)
  	{
  		if(Texto[i] != caracter)  
		  {
  			TextoReemplazado[j] = Texto[i];
        j++;
 		  }
	  }
	
  //Imprime el texto sin el caracter
	printf("\n El texto sin el caracter %c es %s", caracter,TextoReemplazado);
	
  	return 0;
}