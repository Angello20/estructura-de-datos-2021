#include <stdio.h>

int main() {

  //variable
  char texto[100];

  //Pide al usuario un texto y lo guarda
  printf("Escriba su texto: ");
  fgets(texto, 50, stdin);

  //crea un puntero para el largo del texto
  char *largoTexto = strlen(texto);

  //contador para bucle while
  int contador = 0;

  //crea un calloc para reservar memoria dinamica en el heap y relocalizar el texto a la memoria heap
  char *textoHeap = calloc(largoTexto, sizeof(char));

  //bucle para colocar el texto a la memoria heap
  while(texto[contador]!='\0'){
    textoHeap[contador] = texto[contador];
    contador++;
  }

  printf("El texto relocalizado en el Heap es: %s", textoHeap);

  return 0;
}