#include <stdio.h>
int main()
{
  char texto[100] = "";
  char textoinvertido[100] = "";
  int i, j = 0;
  int contador = 0;

  //Pide al usuario el texto que desea invertir
  printf("\nIngrese su texto que desea invertir: %s", texto);
  scanf("%s", texto);

  //contando el largo del texto
  while (texto[contador] != '\0')
  {
    contador++;
  }
  j = contador - 1;

  //Bucle para invertir el texto ingresando caracter por caracter en una nueva cadena de texto
  for (i = 0; i < contador; i++)
  {
    textoinvertido[i] = texto[j];
    j--;
  }

  printf("\nEl texto invertido es: %s", textoinvertido);

  return 0;
}