#include <stdio.h>


int main() {

  //numero para aplicarle la factorial
  int num;
  printf("Digite su numero: ");
  scanf("%i", &num);

  //variable donde se guarda el resultado y un contador para realizar la operación factorial
  int resultado = 1;
  int contador = 2;

  //ciclo para realizar la operación factorial
  while(contador<=num){
      resultado = resultado * contador;
      contador++;
  }

  //Imprime el resultado
  printf("%i",resultado);

  return 0;
}
