#include <stdio.h>

int main() {

  //numero a verificar el largo
  int num;
  printf("Digite su numero: ");
  scanf("%i", &num);
  int contador = 0;

  //ciclo para contar los dígitos del numero
  while(num>=1 || num<=-1){
    num = num / 10; 
    contador++;
  }

  //Imprime el resultado
  printf("El largo del numero es de %i dígitos",contador);
  return 0;
}