#include <stdio.h>

int main() {
  
  //numero para hacer la sumatoria de 0 a n
  int num;
  printf("Digite su numero de sumatoria: ");
  scanf("%i", &num);
  //contador para ciclo
  int i = 0;

  int sumatoria = 0;

  while(i<=num){
    sumatoria = sumatoria + i;
    i++;
  }

 //Imprime el resultado
  printf("La sumatoria da %i",sumatoria);

  return 0;
}