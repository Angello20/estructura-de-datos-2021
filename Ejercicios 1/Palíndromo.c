#include <stdio.h>

int main() {

  //número a invertir
  int num;
  printf("Digite su numero para verificar si es palíndromo: ");
  scanf("%i", &num);
  int numOriginal = num;

  //residuo decimal y la variable a guardar el número invertidp
  int residuos = 0;
  int invertido = 0;
  
  while(num!=0){
    //Guarda los decimales de la división entre el numero dividio entre 10 para así invertir el número
    residuos=num%10;
    //Disminuye el número
    num=num/10;         
    //multiplica el numero anterior para posicionarlo en el valor posicional que le corresponde y le suma el numero siguiente para que quede invertido correctamente
    invertido=invertido*10+residuos;       
  }

  if(invertido==numOriginal){
    printf("El número es palíndromo");
  }
  else{
    printf("El número no es palíndromo");
  }

  return 0;
}