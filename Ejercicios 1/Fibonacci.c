#include "Fibonacci.h"

int Fibonacci(int Fibonacci)
{
    //variables
    int num = 1;
    int anterior = 0;
    int resultado;

    //ciclo para sacar serie FIbonacci
    for (int i = 0; i < Fibonacci; i++){
      //imprime el numero
        printf("%d, ", num);
        //guarda una variable auxiliar
        resultado = num;
        //suma el numero anterior al numero
        num += anterior;
        //guarda el numero anterior para el siguiente ciclo
        anterior = resultado;
    }

    //imprime un espacio en blanco
    printf("\n\n\n");
    return 0;
}