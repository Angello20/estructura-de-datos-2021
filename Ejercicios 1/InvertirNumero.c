/*Invertir numero de hasta 10 digitos*/
#include <stdio.h>

int main(){

  //número a invertir
  int num;
  printf("Digite su numero a invertir: ");
  scanf("%i", &num);

  //residuo decimal y la variable a guardar el número invertidp
  int residuos = 0;
  int invertido = 0;
  
  while(num!=0){
    //Guarda los decimales de la división entre el numero dividio entre 10 para así invertir el número
    residuos=num%10;
    //Disminuye el número
    num=num/10;         
    //multiplica el numero anterior para posicionarlo en el valor posicional que le corresponde y le suma el numero siguiente para que quede invertido correctamente
    invertido=invertido*10+residuos;       
  }

  printf("%d \n",invertido);

  return 0;
}