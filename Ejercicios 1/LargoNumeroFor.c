#include <stdio.h>

int main(void) {

  //numero a verificar el largo
  int num;
  printf("Digite su numero: ");
  scanf("%i", &num);
  int contador = 0;

  //contador
  int i;
  //ciclo para averiguar el largo del numero
  for(i=0; num>=1 || num<=-1; i++){
    num = num / 10; 
  }

  //Imprime el resultado
  printf("El largo del numero es de %i dígitos",i);
  return 0;
}